let configurar = false;
let read = new FileReader();

async function subirS3(subir, credencial, cd){
    try {
        if (!window.AWS) {
            return
        }
        if (!configurar){
            window.AWS.config.update(({region: credencial.region}));
            configurar = true;
        }

        let s3 = new window.AWS.S3({
            credentials: new window.AWS.Credentials({
                apiVersion: 'latest',
                accessKeyId: credencial.accessKeyId,
                secretAccessKey: credencial.secretAccessKey,
                signatureVersion: credencial.signatureVersion,
                region: credencial.region,
                Bucket: credencial.Bucket
            })
        });
        let subirObjeto = await s3.upload({
            Bucket: credencial.Bucket,
            Key: 'File',
            ContentType: document.getElementById("archivoASubir").files[0].type,
            Body: subir
        }).on("httpUploadProgress", function (progreso) {
            console.log("progreso=>", progreso)
            cd(getProgreso(progreso.loaded, progreso.total));
        }).promise();
        console.log("subirObjeto =>", subirObjeto)
        return subirObjeto;
    }
    catch (error) {
        console.log(error)
    }
}

function getProgreso(tamano, tTotal){
    let subida = (tamano / tTotal) * 100;
    return Number(subida.toFixed(0));
}

async function subirArchivo(){
    let credenciales = {
        accessKeyId: 'AKIAUMIW7XPRHW66OENT',
        secretAccessKey: '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',
        signatureVersion: 'v4',
        region: 'us-east-2',
        Bucket: 'practicacc2'
    };
    let pedirArchivos = getArchivo(document.getElementById("archivoASubir").files[0])
    const [archivos] = await Promise.all([
        pedirArchivos
    ])
    await subirS3(archivos, credenciales, (progreso) => {
        console.log(progreso)
    })
}

async function getArchivo(file){
    return new Promise((aceptar, denegar) => {
        read.onload = (e) => {
            aceptar(e.target.result);
        };
        read.onerror = (err) => {
            denegar(false);
        };
        read.readAsArrayBuffer(file);
    });
};
