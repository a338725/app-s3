<h3>Código para subir archivos a AWS S3</h3>
Este código permite subir archivos a AWS S3 utilizando JavaScript y la SDK de AWS. Para usarlo, siga los siguientes pasos:

1. Se crea un archivo HTML con un botón para seleccionar un archivo y otro botón para iniciar la subida.

2. Incluya la biblioteca de AWS SDK en su archivo HTML:
<script src="https://sdk.amazonaws.com/js/aws-sdk-2.966.0.min.js"></script>

3. Se crea otro archivo de tipo JS donde se configura la conexion a AWS S3 y se colocan las credenciales necesarias para lograr la conexion como el ACCESSKEYID y SECRETACCESSKEY, ademas del nombre del bucket y la region en la cual esta ubicado.

4. Se utiliza el codigo en JavaScript llamado por el segundo boton para subir el archivo y confirmar la operacion.

5. El boton configura de tal forma que se manda a llamar la operacion desde JavaScript:
<button onClick="subirArchivo()"> Subir archivo</button>



